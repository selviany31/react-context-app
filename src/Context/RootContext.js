import React, { createContext, useState } from 'react'

export const RootContext = createContext()
export const RootContextConsumer = RootContext.Consumer

export default function RootContextProvider({ children }) {
	const [biodata, setBiodata] = useState({ name: 'No Data ', age: 'No Data' })

	return <RootContext.Provider value={{ biodata, setBiodata }}>{children}</RootContext.Provider>
}
